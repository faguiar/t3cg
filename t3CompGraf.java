//Trabalho de Computação Gráfica - Felipe Aguiar Corrêa

import javax.vecmath.*;
import javax.media.j3d.*;
import javax.swing.JFrame;
import java.util.Hashtable;
import java.util.Enumeration;
import com.sun.j3d.utils.image.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.loaders.*;
import com.sun.j3d.loaders.objectfile.*;
import com.sun.j3d.utils.behaviors.vp.*;
import com.sun.j3d.utils.universe.*;
import java.io.FileInputStream;




public class t3CompGraf extends JFrame
{
	public Canvas3D myCanvas3D;
	
	public t3CompGraf(){
		
		//Fecha a janela
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		myCanvas3D = new Canvas3D(SimpleUniverse.getPreferredConfiguration());

		SimpleUniverse simpUniv = new SimpleUniverse(myCanvas3D);

		//posicao default do visualizador
		simpUniv.getViewingPlatform().setNominalViewingTransform();
		
		//Create Audio Device
		AudioDevice ad = simpUniv.getViewer().createAudioDevice();


		//gera a cena
		createSceneGraph(simpUniv);

		//adiciona luz a cena
		addLight(simpUniv);

		//navegacao do mouse
		OrbitBehavior ob = new OrbitBehavior(myCanvas3D);
		ob.setSchedulingBounds(new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE));
		simpUniv.getViewingPlatform().setViewPlatformBehavior(ob);

		
		//mostra a janela
		setTitle("Trabalho 3 de CG - Felipe Correa");
		setSize(1024,900);
		getContentPane().add("Center", myCanvas3D);
		setVisible(true);

  }
  
  public static void main(String[] args){
     t3CompGraf le = new t3CompGraf();
  }
  
    //objetos gerados são adicionados para o SimpleUniverse
  public void createSceneGraph(SimpleUniverse su){
	//carrega textura do prédio e da Antena
    TextureLoader textureLoad_predio = new TextureLoader("buildintexture.jpg",null);
	TextureLoader textureLoad_antena = new TextureLoader("antenaTexture.png", null);
    
    //Load and define the sounds.
    MediaContainer soundMC;
    BackgroundSound soundBG = new BackgroundSound();
    try
    {
      FileInputStream bgis = new FileInputStream("airplane.wav");
      soundMC = new MediaContainer(bgis);
      soundBG.setSoundData(soundMC);
    }
    catch(Exception e)
    {
      System.out.println("Problema ao carregar arquivo de som");
    }


    soundBG.setEnable(true);
    soundBG.setLoop(Sound.INFINITE_LOOPS);
    soundBG.setInitialGain(0.6f);


    BoundingSphere sbounds = new BoundingSphere (new Point3d(0.0,0.0,0.0),Double.MAX_VALUE);
    soundBG.setSchedulingBounds(sbounds);
	
    //Gera uma imagem para ser usada na textura do prédio e da antena. Deve ser em potência de 2
    //deixar 256 para uma resolução boa
    ImageComponent2D textureIm = textureLoad_predio.getScaledImage(512,512);
    ImageComponent2D textureIm_ant = textureLoad_antena.getScaledImage(64,64);
	
	//gera a textura.
    Texture2D aTexture = new Texture2D(Texture2D.BASE_LEVEL,Texture2D.RGB,textureIm.getWidth(),textureIm.getHeight());
    aTexture.setImage(0,textureIm);
    Texture2D bTexture = new Texture2D(Texture2D.BASE_LEVEL,Texture2D.RGB,textureIm_ant.getWidth(),textureIm_ant.getHeight());
    bTexture.setImage(0,textureIm_ant);

    //definição da Appearence para ser aplicada ao prédio
    Appearance textureApp = new Appearance();
    textureApp.setTexture(aTexture);
    TextureAttributes textureAttr = new TextureAttributes();
    textureAttr.setTextureMode(TextureAttributes.REPLACE);
    textureApp.setTextureAttributes(textureAttr);
    Material mat = new Material();
    mat.setShininess(-20.0f);
    textureApp.setMaterial(mat);
    TexCoordGeneration tcg = new TexCoordGeneration(TexCoordGeneration.OBJECT_LINEAR, TexCoordGeneration.TEXTURE_COORDINATE_2);

    textureApp.setTexCoordGeneration(tcg);
    
	//Definição da Appearance para ser aplicado à antena do prédio
    Appearance textureApp_ant = new Appearance();
    textureApp_ant.setTexture(bTexture);
    TextureAttributes textureAttr_ant = new TextureAttributes();
    textureAttr_ant.setTextureMode(TextureAttributes.REPLACE);
    textureApp_ant.setTextureAttributes(textureAttr_ant);
    Material mat_ant = new Material();
    mat_ant.setShininess(-20.0f);
    textureApp_ant.setMaterial(mat_ant);
    TexCoordGeneration tcg_ant = new TexCoordGeneration(TexCoordGeneration.OBJECT_LINEAR, TexCoordGeneration.TEXTURE_COORDINATE_2);

    textureApp_ant.setTexCoordGeneration(tcg_ant);


    //tBuilding é um cubo esticado, formando um paralelepipedo que será
    //usado como um prédio. É aplicado a textureApp nele
    Box tBuilding = new Box(2f,4f,0.4f,textureApp);
    Transform3D tfBox = new Transform3D();
    tfBox.rotY(Math.PI/8);
    Transform3D rotationX = new Transform3D();
    rotationX.rotX(-Math.PI);
    tfBox.mul(rotationX);
	
	//Translação para encaixar o prédio no background
    tfBox.setTranslation(new Vector3f(0.0f,-5f,-8.4f));

    //tfBox é o transformation group para o prédio (tgBuilding).
    TransformGroup tgBuilding = new TransformGroup(tfBox);
    
    //Adiciona tBuilding para o transformation group tgBuilding
    tgBuilding.addChild(tBuilding);
    
    //Cria a antena sinalizadora pro prédio
    Box tAntena = new Box(0.03f,3f,0.1f,textureApp_ant);
    Transform3D tfAntena = new Transform3D();
    tfBox.rotY(Math.PI/8);
    Transform3D ant_rotationX = new Transform3D();
    ant_rotationX.rotX(-Math.PI);
    tfAntena.mul(ant_rotationX);
    //Translation pra encaixar a Antena no topo do prédio
    tfAntena.setTranslation(new Vector3f(0.0f,-2f,-8.4f));
    
    TransformGroup tgAntena = new TransformGroup(tfAntena);
    //Adiciona no TransformGroup a Antena
    tgAntena.addChild(tAntena);

	//Appearance pra ser usada na lâmpada sinalizadora
    Appearance tAntenaTop_App = new Appearance();

    tAntenaTop_App.setMaterial(new Material(new Color3f(0.3f,0.0f,0.0f), new Color3f(1.0f,0.0f,0.0f), new Color3f(0.6f,0.0f,0.0f), new Color3f(0.8f,0.0f,0.0f), 900.0f));
	//Gera a transparência interpolada
    TransparencyAttributes ta1 = new TransparencyAttributes();
    ta1.setTransparencyMode(TransparencyAttributes.BLENDED);
    ta1.setTransparency(0.4f);
	//Adiciona a transparência interpolada no tAntenaTop_App
    tAntenaTop_App.setTransparencyAttributes(ta1);

    //Cria a esfera pra receber a luz sinalizadora no topo da antena e dá a Appearance transparente à ela
    Sphere tAntenaTop = new Sphere(0.05f,Sphere.GENERATE_NORMALS,100,tAntenaTop_App);
    Transform3D tfAntenaTop = new Transform3D();
    tfAntenaTop.setTranslation(new Vector3f(0.0f,-3f,-0.07f));
    //Adiciona ao transformGroup da Antena
    TransformGroup tgAntenaTop = new TransformGroup(tfAntenaTop);
    tgAntenaTop.addChild(tAntenaTop);
    
    //Adiciona a lâmpada da Antena no TransformGroup da Antena:  tgAntena[tfAntena, tgAntenaTop[tfAntenaTop]]
    tgAntena.addChild(tgAntenaTop);
    
    //carrega o obj.
    ObjectFile f = new ObjectFile(ObjectFile.RESIZE);
    Scene s = null;
    try{
      s = f.load("toyplane.obj");
    }
    catch (Exception e){
      System.out.println("Não foi possível carregar arquivo:" + e);
    }

    //Transformacao para o grupo do objeto carregado, incluindo animação do objeto inteiro
    Transform3D tfObject = new Transform3D();
    tfObject.rotZ(0.4*Math.PI);
    Transform3D xRotation = new Transform3D();
    xRotation.rotY(0.4*Math.PI);
    tfObject.mul(xRotation);
	
	tfObject.setTranslation(new Vector3f(0.0f,0.0f,0.0f));
        
    TransformGroup tgObject = new TransformGroup(tfObject);
    tgObject.addChild(s.getSceneGroup());


    //Printa o nome das partes do obj. Apenas para debug e para saber quais partes eu vou precisar aplicar textura/cores
    Hashtable namedObjects = s.getNamedObjects();
    Enumeration enumer = namedObjects.keys();
    String name;
    while (enumer.hasMoreElements())
    {
      name = (String) enumer.nextElement();
      System.out.println("Name: "+name);
    }

    //Aparências do aviãozinho
    //Piloto azul e textura para os outros (logo mais)
    Appearance lightBlueApp = new Appearance();
    setToMyDefaultAppearance(lightBlueApp,new Color3f(0.0f,0.1f,0.3f));
    
    //lightBlueApp (azul claro) é aplicado nos rigs, nas caudas e no piloto do avião
    Shape3D planepilot = (Shape3D) namedObjects.get("planepilot_mesh");
    Shape3D planerigr = (Shape3D) namedObjects.get("planerigr_mesh");
    Shape3D planerigl = (Shape3D) namedObjects.get("planerigl_mesh");
    Shape3D planetaill = (Shape3D) namedObjects.get("planetaill_mesh");
    Shape3D planetailr = (Shape3D) namedObjects.get("planetailr_mesh");
    
    planepilot.setAppearance(lightBlueApp);
    planerigr.setAppearance(lightBlueApp);
    planerigl.setAppearance(lightBlueApp);
    planetaill.setAppearance(lightBlueApp);
    planetailr.setAppearance(lightBlueApp);
    
	//Appearance planeYellow é aplicada ao corpo do avião e as asas
	Appearance planeYellow = new Appearance();
	setToMyDefaultAppearance(planeYellow, new Color3f(0.4f,0.4f,0.2f));
	
	Shape3D planeBody = (Shape3D) namedObjects.get("planebody_mesh");
	Shape3D planeWingTop = (Shape3D) namedObjects.get("planewingtop_mesh");
	Shape3D planewingbottomr = (Shape3D) namedObjects.get("planewingbottomr_mesh");
	Shape3D planewingbottoml = (Shape3D) namedObjects.get("planewingbottoml_mesh");
	
	planeBody.setAppearance(planeYellow);
	planeWingTop.setAppearance(planeYellow);
	planewingbottomr.setAppearance(planeYellow);
	planewingbottoml.setAppearance(planeYellow);
	
	//Hélices do avião para vermelho (objeto planepropeller)
	Appearance planeRed = new Appearance();
	setToMyDefaultAppearance(planeRed, new Color3f(0.8f,0.0f,0.0f));
	Shape3D planepropeller = (Shape3D) namedObjects.get("planepropeller_mesh");
    planepropeller.setAppearance(planeRed);

    ////O TransformationGroup para o propeller do avião, onde será implementado o giro das hélices frontais
    //Retiro planepropeller do SceneGraph s antes de fazer isso
	BranchGroup new_s = s.getSceneGroup();
    new_s.removeChild(planepropeller);
    
    TransformGroup tgPropeller = new TransformGroup();
    tgPropeller.addChild(planepropeller);
    	
	//Rotacao do Propeller
	Transform3D propellerRotationAxis = new Transform3D();
	int timeStartPropeller = 0; //O propeller começa a rodar em 1s
	int numRotation = 400;  //Numero de rotaçoes a serem feitas
	int timeRotation = 100;//Uma rotacao demora 1.5 s para ser completa

	
	//alpha para a rotacao.
    Alpha propellerRotAlpha = new Alpha(numRotation, Alpha.INCREASING_ENABLE, timeStartPropeller, 0,timeRotation,0,0,0,0,0);
    float[] knots_prop = { 0.0f, 1.0f };
    Quat4f[] quats_prop = new Quat4f[2];

    quats_prop[0] = new Quat4f(0.0f, 0.0f, 0.0f, 1f);
    quats_prop[1] = new Quat4f((float) Math.PI*2, 0.0f, 0.0f, 1f);
  
    //Definicoes da rotacao.
    RotationPathInterpolator propellerRotation = new RotationPathInterpolator(propellerRotAlpha,tgPropeller,propellerRotationAxis,knots_prop, quats_prop );

    BoundingSphere prop_bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE);
    propellerRotation.setSchedulingBounds(prop_bounds);
    //Seta capacidade de transform pro group tgPropeller e adiciona a rotacao pra ele
    tgPropeller.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	tgPropeller.addChild(propellerRotation); 
	
	//Adiciona o propeller de volta ao Avião
	//tgObject.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	tgObject.addChild(tgPropeller);
	
	//Grupo da cena
    BranchGroup theScene = new BranchGroup();

    //Definições do Background
    BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE);
    BoundingSphere fbounds = new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE);
    //carrega a imagem do background.
    TextureLoader textureLoad = new TextureLoader("rsz_bg.jpg",null);
    //Define a imagem como background e a adiciona na cena.
    Background bgImage = new Background(textureLoad.getImage());
    bgImage.setApplicationBounds(bounds);
	
	//Position Interpolator no objeto AVIÃO
	Alpha alphaFlight = new Alpha(-1,12000);
	Transform3D axisOfTranslation = new Transform3D();
	//axisOfTranslation.rotY(0);
	//axisOfTranslation.setTranslation(new Vector3f(0.0f,0.0f,20.0f));
	
	//Pontos de interpolação das positions
    float[] knots = { 0.0f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1.0f };
    Point3f[] positions = new Point3f[11];
    Quat4f[] quats = new Quat4f[11];
    AxisAngle4f axis = new AxisAngle4f(0.0f, 0.0f, 0.0f, 0.0f);
    axisOfTranslation.set(axis);

    quats[0] = new Quat4f(0.0f, 0.0f, 0.0f, 1f);
    quats[1] = new Quat4f(0.2f, 0.0f, 0.0f, 1f);
    quats[2] = new Quat4f(0.4f, 0.0f, 0.0f, 1f);
    quats[3] = new Quat4f(0.2f, 0.0f, 0.0f, 1f);
    quats[4] = new Quat4f(0.0f, 0.0f, 0.0f, 1f);
    quats[5] = new Quat4f(0.2f, 0.0f, 0.0f, 1f);
    quats[6] = new Quat4f(0.4f, 0.0f, 0.0f, 1f);
    quats[7] = new Quat4f(0.3f, 0.0f, 0.0f, 1f);
    quats[8] = new Quat4f(0.2f, 0.0f, 0.0f, 1f);
    quats[9] = new Quat4f(2f, 0.0f, 0.0f, 1f);
    quats[10] = new Quat4f(0.0f, 0.0f, 0.0f, 1f);
    
	//Linha no eixo tridimensional que vai ser o PATH a ser percorrido
    positions[0] = new Point3f(-15.0f ,2.0f ,-20f);
    positions[1] = new Point3f(-10.0f ,2.0f ,-20f);
    positions[2] = new Point3f(-5.0f ,2.0f ,-20f);
    positions[3] = new Point3f(0.0f,2.0f ,-20f);
    positions[4] = new Point3f(5.0f ,2.0f ,-20f);
    positions[5] = new Point3f(10.0f ,2.0f ,-20f);
    positions[6] = new Point3f(15.0f ,2.0f ,-20f);
    positions[7] = new Point3f(20.0f ,2.0f ,-20f);
    positions[8] = new Point3f(25.0f ,2.0f ,-20f);
    positions[9] = new Point3f(30.0f ,2.0f ,-20f);
    positions[10] = new Point3f(35.0f ,2.0f ,-20f);
    //Path Interpolator
	RotPosPathInterpolator flight_interpolator = new RotPosPathInterpolator(alphaFlight,tgObject,axisOfTranslation,knots, quats, positions);
	
	//Adiciona Animação ao TransformGroup do avião
	flight_interpolator.setSchedulingBounds(fbounds);
	tgObject.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	tgObject.addChild(flight_interpolator);
	
	//Adiciona avião na cena na cena
    theScene.addChild(tgObject);

    //adiciona background e a building no thescene 
    theScene.addChild(bgImage);
    theScene.addChild(tgBuilding);
    theScene.addChild(tgAntena);
	//adiciona som de bg
	theScene.addChild(soundBG);
	
    theScene.compile();

    //Adiciona a cena ao SimpleUniverse
    su.addBranchGraph(theScene);
  }

  public static void setToMyDefaultAppearance(Appearance app, Color3f col){
    app.setMaterial(new Material(col,col,col,col,150.0f));
  }

  //Definicoes da luz
  public void addLight(SimpleUniverse su){
	
	BranchGroup bgLight = new BranchGroup();
    BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), Double.MAX_VALUE);
    
	//luz dinamica
    Color3f lightColourSpot = new Color3f(0.2f, 0.2f,0.2f);
    SpotLight lightSpot = new SpotLight(lightColourSpot,new Point3f(0.0f,-7f,0.01f),new Point3f(0.01f,0.01f,0.01f),new Vector3f(0.2f,0.2f,0.2f),0.1f,0.0f);

    lightSpot.setInfluencingBounds(bounds);
	
	Color3f pointLightAntenaColor = new Color3f(0.7f,0.0f,0.0f);
	PointLight antenaLight = new PointLight(pointLightAntenaColor, new Point3f(0.0f,-3f,-0.07f), new Point3f(10f,10f,10f));
	
	//TransformGroup parra luz da antena
	TransformGroup tfantenaLight = new TransformGroup();
	
	tfantenaLight.addChild(antenaLight);
    //TransformGroup pra luz dinamica
    TransformGroup tfmLight = new TransformGroup();
    tfmLight.addChild(lightSpot);
	
    //alpha da rotacao
    Alpha alphaLight = new Alpha(-1,4000);
    //Rotacao 1 e 2 (ida e volta)
    RotationInterpolator rot = new RotationInterpolator(alphaLight,tfmLight,new Transform3D(),0.0f,(float) Math.PI*2);
	
	rot.setSchedulingBounds(bounds);
	
    tfmLight.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
    tfmLight.addChild(rot);
	
    bgLight.addChild(tfmLight);
	bgLight.addChild(tfantenaLight);
    
    //Luz ambiente.
    Color3f ambientLightColour = new Color3f(0.5f, 0.5f, 0.5f);
    AmbientLight ambLight = new AmbientLight(ambientLightColour);
    ambLight.setInfluencingBounds(bounds);
    bgLight.addChild(ambLight);
    
    su.addBranchGraph(bgLight);
  }
}
